<?php

/**
 * @file
 * Home of the FeedsPushFetcher class.
 */

//require_once 'FeedsHTTPFetcher.inc';

/**
 * Fetches data via Push.
 */
class FeedsPuSHFetcher extends FeedsHTTPFetcher {
  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'auto_detect_feeds' => FALSE,
      'use_pubsubhubbub' => TRUE,
      'default_hub' => '',
    );
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = array();
    $form['default_hub'] = array(
      '#type' => 'textfield',
      '#required' => true,
      '#title' => t('Default hub'),
      '#description' => t('Enter the URL of the default PubSubHubbub hub (e. g. superfeedr.com). If given, this hub will be used when none is found in the specified feed.'),
      '#default_value' => $this->config['default_hub'],
    );
    return $form;
  }

  /**
   * Implement FeedsFetcher::subscribe() - subscribe to hub.
   */
  public function subscribe(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    $url = url($this->path($source->feed_nid), array('absolute' => TRUE));

    $sub = $this->subscriber($source->feed_nid);
    // subscribe using hub provided in the source
    $sub->subscribe($source_config['source'], $url);

    // if that didn't work, subscribe using default hub
    if (is_null($sub->subscription()))
        $sub->subscribe($source_config['source'], $url, $this->config['default_hub']);
  }

  /**
   * Expose source form.
   */
  public function sourceForm($source_config) {
    $form = array();
    $form['source'] = array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#description' => t('Enter a feed URL.'),
      '#default_value' => isset($source_config['source']) ? $source_config['source'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );

    $form['threshold'] = array(
      '#type' => 'textfield',
      '#title' => t('Threshold'),
      '#description' => t('Enter threshold (max items per day).'),
      '#default_value' => empty($source_config['threshold'])
        ? variable_get('rymfeeds_default_threshold', 50)
        : $source_config['threshold'],
      '#maxlength' => 5,
      '#required' => TRUE,
    );

    return $form;
  }

  /**
   * Override parent::sourceFormValidate().
   */
  public function sourceFormValidate(&$values) {
    parent::sourceFormValidate($values);
    $values['threshold'] = (int)$values['threshold'];
  }
}

